<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://inkandwater.co.uk
 * @since      1.0.0
 *
 * @package    IW_Site_Maintenance
 * @subpackage IW_Site_Maintenance/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
