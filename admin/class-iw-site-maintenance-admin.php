<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://inkandwater.co.uk
 * @since      1.0.0
 *
 * @package    IW_Site_Maintenance
 * @subpackage IW_Site_Maintenance/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    IW_Site_Maintenance
 * @subpackage IW_Site_Maintenance/admin
 * @author     Ink & Water LTD <tom@inkandwater.co.uk>
 */
class IW_Site_Maintenance_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
        $this->settings_page = 'iw-site-maintenance-settings';

	}

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in IW_Site_Maintenance_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The IW_Site_Maintenance_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

        wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/iw-site-maintenance-admin.css', array(), $this->version, 'all' );

    }

    /**
     * Add settings action link to the plugins page.
     *
     * @since    1.0.0
     */
    public function add_action_links( $links ) {

        $links[] =  sprintf( '<a href="%1s">%2s</a>', 
                        esc_url( admin_url( "options-general.php?page={$this->settings_page}" ) ),
                        __( 'Settings', $this->plugin_name ) 
                    );

        return $links;

    }

    /**
     * Add options page
     */
    public function setup_settings_page() {

        add_options_page( 
            'Site Maintenance Settings', 
            'Site Maintenance', 
            'manage_options', 
            $this->settings_page,
            array( &$this, 'create_settings_page' )
        );

    }

    public function create_settings_page() {
        ?>
            <form action='options.php' method='post'>

                <h2>Site Maintenance Settings</h2>

                <?php
                settings_fields( 'iw-site-maintenance' );
                do_settings_sections( 'iw-site-maintenance' );
                submit_button();
                ?>

            </form>
        <?php

    }

    public function settings_init() { 

        register_setting( 'iw-site-maintenance', 'iw-site-maintenance-settings' );

        add_settings_section(
            'IW_pluginPage_section', 
            __( 'Your section description', 'iw-site-maintenance' ), 
            array( &$this, 'IW_settings_section_callback' ), 
            'iw-site-maintenance'
        );

        add_settings_field( 
            'checkbox-field', 
            __( 'Activate or Deactivate', 'iw-site-maintenance' ), 
            array( &$this, 'checkbox_field_render' ), 
            'iw-site-maintenance', 
            'IW_pluginPage_section' 
        );

    }

    public function checkbox_field_render() { 

        $options = get_option( 'iw-site-maintenance-settings' );
        ?>

        <input type="checkbox" id="switch" />
        <label for="switch">Toggle</label>
        
        <?php

    }

    public function IW_settings_section_callback() { 

        echo __( 'This section description', 'iw-site-maintenance' );

    }


}
