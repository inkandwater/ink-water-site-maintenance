<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://inkandwater.co.uk
 * @since      1.0.0
 *
 * @package    IW_Site_Maintenance
 * @subpackage IW_Site_Maintenance/public/partials
 */
?>

<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo get_bloginfo( 'name' ); ?> | Scheduled Maintenance</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>

            html, body {
                height: 100%;
                background-color: #FFFFFF;
                color: #0A0A0A;
            }

            body {
                text-align: center;
                height: 100%;
                margin: 0;
                font-family: "Helvetica",Helvetica,Roboto,Arial,sans-serif;
            }

            img {
                display: block;
                margin: 1em auto 5em;
                max-width: 100%;
            }

            h1,h2,h3,h4,h5,h6 {

                color: #414757;
                margin: 0 auto;
                display: inline-block;
                line-height: 1.3em;
                word-wrap: break-word;
            }

            .message-container {
                display: table;
                width: 100%;
                height: 100%;
            }

            .message {
                display: table-cell;
                width: 100%;
                vertical-align: middle;
                padding: 1em;
            }

        </style>

    </head>

    <body>
        <div class="message-container">
            <div class="message">

                <img src="https://scrgrowthhub.staging.wpengine.com/wp-content/themes/iw-scrgh/img/scrgrowthhublogo.svg" alt="SCR Growth Hub Logo"/>

                <h1>Scheduled Maintenance</h1>
                <p>Our website is undergoing scheduled maintenance, please check back soon.</p>
                <p>We apologise for any inconvenience caused.</p>

            </div>
        </div>

    </body>
</html>