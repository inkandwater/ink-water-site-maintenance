<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://inkandwater.co.uk
 * @since      1.0.0
 *
 * @package    IW_Site_Maintenance
 * @subpackage IW_Site_Maintenance/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    IW_Site_Maintenance
 * @subpackage IW_Site_Maintenance/includes
 * @author     Ink & Water LTD <tom@inkandwater.co.uk>
 */
class IW_Site_Maintenance_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'iw-site-maintenance',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
