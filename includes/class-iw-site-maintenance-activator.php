<?php

/**
 * Fired during plugin activation
 *
 * @link       https://inkandwater.co.uk
 * @since      1.0.0
 *
 * @package    IW_Site_Maintenance
 * @subpackage IW_Site_Maintenance/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    IW_Site_Maintenance
 * @subpackage IW_Site_Maintenance/includes
 * @author     Ink & Water LTD <tom@inkandwater.co.uk>
 */
class IW_Site_Maintenance_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
